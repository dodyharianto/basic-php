<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function displaySignUpPage()
    {
        return view('form');
    }
    public function displayWelcomePage()
    {
        return view('welcome-page');
    }
}
