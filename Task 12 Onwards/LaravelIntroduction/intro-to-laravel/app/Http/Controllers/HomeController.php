<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function displayHome()
    {
        // Display the index.php webpage
        return view('index');
    }
}
