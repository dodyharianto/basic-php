<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sign Up Form</title>
    <h1>Buat Account Baru!</h1>
</head>
<body>
    <h2>Sign Up Form</h2>
    <form action="/welcome-page">
        <label>First name:</label>
        <br><br>
        <input type="text">
        <br><br>

        <label>Last name:</label>
        <br><br>
        <input type="text">
        <br><br>

        <label>Gender:</label>
        <br><br>
        <input type="radio" name="gender"> Male <br>
        <input type="radio" name="gender"> Female <br>
        <input type="radio" name="gender"> Other <br>
        <br><br>

        <label>Nationality:</label>
        <br><br>
        <select type="nationality">
            <option value="Indonesian">Indonesian</option>
            <option value="Other">Other</option>
        </select>
        <br><br>

        <label>Language Spoken:</label>
        <br><br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br>
        <br>

        <label>Bio:</label>
        <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea>
        <br><br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>