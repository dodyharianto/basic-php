<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Display the Laravel welcome page (default)
// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [HomeController::class, 'displayHome']);
Route::get('/register', [AuthController::class, 'displaySignUpPage']);
Route::get('/welcome-page', [AuthController::class, 'displayWelcomePage']);

// Note: The route is named /welcome-page instead of /welcome since it already exists (laravel default welcome page).

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/table', function(){
    return view('layout.table');
});

Route::get('/data-tables', function(){
    return view('layout.data-tables');
});